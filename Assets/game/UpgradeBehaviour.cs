﻿// UpgradeBehaviour.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class UpgradeBehaviour : MonoBehaviour
{
	public int shopID;
	int grade;
	int nextCost;

	public void SetStore() {
		grade = Scenes.upgrade[shopID];
		setGradeAndCost();
	}

	void setGradeAndCost() {
		nextCost = Mathf.FloorToInt((grade+1) * Mathf.Pow(2, grade+4)/4);

		this.GetComponentInChildren<Text>().text = "$";
		for (int i=4; i!=nextCost.ToString().Length; i--) {
			this.GetComponentInChildren<Text>().text += "  ";
		}
		this.GetComponentInChildren<Text>().text += nextCost.ToString();
	}

	public void GreadUp() {
		if (nextCost > Scenes.maryoku)
			return;
		Scenes.maryoku -= nextCost;
		Scenes.upgrade[shopID]++;
		grade++;
		setGradeAndCost();
		PlayerPrefs.SetInt("upgrade_"+shopID.ToString(), grade);
		Scenes.DataUpdate();
		Scenes.CreateSound("SE/shop_cash");

		Debug.Log("upgrade_"+shopID.ToString()+" = "+grade);
	}
}
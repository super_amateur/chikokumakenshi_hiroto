﻿// ScenesMaster.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class ScenesMaster : MonoBehaviour {

	[HeaderAttribute("0 : Title")]
	[HeaderAttribute("1 : Game")]
	[HeaderAttribute("2 : Result")]
	[HeaderAttribute("3 : Shop")]
	[HeaderAttribute("4 : Setting")]
	[HeaderAttribute("5 : Pause(in Game)")]
	public int startSceneNm;
	public float limitTime;

	[SpaceAttribute]
	public bool setMaryoku;
	public int setMaryokuVal;
	public bool setUpgrade;
	public int[] setUpgradeVal = new int[3];

	public UnityEngine.Audio.AudioMixerGroup VoiceGroup;

	[SpaceAttribute]

	[SerializeField]
	bool DebugGUI = false;
	[SerializeField]
	MaskableGraphic[] raycastObject;
	
	void Start() {
		Scenes.GUISys = GameObject.Find("Canvas").GetComponentsInChildren<GUISystem>();
		int i_01 = 0;
		foreach(MaskableGraphic obj in Scenes.GUISys[0].transform.parent.GetComponentsInChildren<MaskableGraphic>()) { if (obj.raycastTarget) i_01++; }
		// raycastObject = new MaskableGraphic[i_01+1];
		Scenes.raycastObject = new MaskableGraphic[i_01];
		i_01 = 0;
		foreach(MaskableGraphic obj in Scenes.GUISys[0].transform.parent.GetComponentsInChildren<MaskableGraphic>())
		{
			if (obj.raycastTarget) {
				// raycastObject[i_01] = obj;
				Scenes.raycastObject[i_01] = obj;
				i_01++;
			}
		}

		Scenes.player = GameObject.Find("Player").GetComponent<PlayerOperation>();
		Scenes.road = GameObject.Find("Roads").GetComponent<Roads>();

		Scenes.VoiceGroup = mixer.FindMatchingGroups("Voice")[0];

		Scenes.maryoku = PlayerPrefs.GetInt("maryoku", 0);
		Scenes.upgrade = new int[]{
			PlayerPrefs.GetInt("upgrade_0", 0),
			PlayerPrefs.GetInt("upgrade_1", 0),
			PlayerPrefs.GetInt("upgrade_2", 0)
		};

		if (setMaryoku) {
			Scenes.maryoku = setMaryokuVal;
			PlayerPrefs.SetInt("maryoku", setMaryokuVal);
			Scenes.DataUpdate();
		}
		if (setUpgrade) {
			Scenes.upgrade = setUpgradeVal;
			PlayerPrefs.SetInt("upgrade_0", setUpgradeVal[0]);
			PlayerPrefs.SetInt("upgrade_1", setUpgradeVal[1]);
			PlayerPrefs.SetInt("upgrade_2", setUpgradeVal[2]);
			Scenes.DataUpdate();
		}

		// ボリュームの読み込み
		foreach (Slider slider in GameObject.Find("4_Setting").GetComponentsInChildren<Slider>()) {
			if (slider.name == "SE") {
				mixer.SetFloat("Voice", ((PlayerPrefs.GetFloat("AudioVolSE", 0.0f)+80)*9/8)-80);
			}
			slider.value = PlayerPrefs.GetFloat("AudioVol"+slider.name, 0.0f);
		}
	}

	bool start;
	void Update () {

		if (!start) {
			Scenes.SceneNm = startSceneNm;
			start = true;
		}
		Scenes.time += Time.deltaTime * Scenes.speed;
		Scenes.limitTime = limitTime;

		foreach (AudioSource AS in GameObject.Find("BGMMaster").GetComponentsInChildren<AudioSource>())
		{
			if (AS.gameObject == GameObject.Find("BGMMaster")) continue;
			if (!AS.isPlaying)
			{
				Destroy(AS.gameObject);
			}
		}

		// デバッグモード移行
		if (Input.GetKey(KeyCode.LeftCommand) & Input.GetKey(KeyCode.LeftShift) & Input.GetKeyDown(KeyCode.J))
		{
			DebugGUI = !DebugGUI;
		}
	}
	
	public void SetSceneNm (int a) {
		Scenes.SceneNm = a;
	}

	// ボリューム制御

	[SerializeField]
	UnityEngine.Audio.AudioMixer mixer;

	public float ChangeVolumeBGM
	{
		set {ChangeVolumeMaster("BGM", value);}
	}
	public float ChangeVolumeSE
	{
		set {ChangeVolumeMaster("SE", value);}
	}
	public void CreateSound(string audio)
	{
		 Scenes.CreateSound(audio);
	}
	void ChangeVolumeMaster(string a, float b) {
		mixer.SetFloat(a, b);
		PlayerPrefs.SetFloat("AudioVol"+a, b);
		if (a == "SE") {
			mixer.SetFloat("Voice", ((b+80)*9/8)-80);
		}
		Scenes.DataUpdate();
	}

	public void SetClearStatus(int status) {
		Scenes.clearStatus = status;
		/*
		 * -1: null
		 *  0: clear
		 *  1: filed
		 *  2: exit
		 */
	}

	public void ResetUpgradeData()
	{
		Scenes.upgrade[0] = 0;
		Scenes.upgrade[1] = 0;
		Scenes.upgrade[2] = 0;
		Scenes.DataUpdate();
	}

	void OnGUI()
	{
		if (!DebugGUI) return;

		const int labelHeight = 20;
		int labelY = 10;

        // Make a background box
        GUI.Box(new Rect(10,10,300,90), "Debug GUI");

		GUI.Label (new Rect (20, labelY+=labelHeight, 280, labelHeight), "Scenes.speed = "+Scenes.speed);
		GUI.Label (new Rect (20, labelY+=labelHeight, 280, labelHeight), "Scenes.speedMultip = "+Scenes.speedMultip);

	}
}
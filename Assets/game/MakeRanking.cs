// MakeRanking.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class MakeRanking : MonoBehaviour
{
	[SerializeField]
	GameObject ranking;
	[SerializeField]
	Transform parent;

	Post post;

	bool correct = false;

	void Start() {
		post = GetComponent<Post>();
	}

	public void Reset() {
		correct = false;
	}
	
	void Update () {
		if (!correct) {
			if (post.rankings.Length > 1 & post.rankings.Length <= 16) {
				foreach (RectTransform rt in GetComponentsInChildren<RectTransform>()) {
					if (rt.gameObject.name.IndexOf("(Clone)") != -1) {
						Destroy(rt.gameObject);
					}
				}
				for (int i=0; i<post.rankings.Length-1; i++) {
					// Debug.Log("ランキングラン作るよ！(｀・ω・´)");
					GameObject obj = Instantiate(ranking);
					// obj.transform.parent = parent;
					obj.GetComponent<RectTransform>().SetParent(parent);
					obj.transform.FindChild("Number").GetComponentInChildren<Text>().text = (i+1).ToString();
					obj.transform.FindChild("Name").GetComponentInChildren<Text>().text = post.rankings[i].Split('|')[0].Split(':')[1];
					obj.transform.FindChild("Score").GetComponentInChildren<Text>().text = post.rankings[i].Split('|')[1].Split(':')[1];
					obj.GetComponent<RectTransform>().localPosition = new Vector3(119, -80f - (i+1)*25, 0);
					obj.GetComponent<RectTransform>().localScale = Vector3.one;
				}
				correct = true;
			}
		}
	}
}
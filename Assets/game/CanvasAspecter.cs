﻿// CanvasAspecter.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class CanvasAspecter : MonoBehaviour {

	void Start () {
		this.transform.position = Vector3.zero;
		
		foreach (Canvas cvs in this.GetComponentsInChildren<Canvas>()) {
			RectTransform RT = cvs.GetComponent<RectTransform>();
			Vector2 size = RT.sizeDelta;
			size.y /= (((float)Screen.width/(float)Screen.height) * (16f/9f));
			RT.sizeDelta = size;
		}
	}
}

// ObjectBehaviour.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class ObjectBehaviour : MonoBehaviour {
	
	
	public Rigidbody2D rig;
	public Homoh homo;
	
	void Start() {
		rig = this.gameObject.GetComponent<Rigidbody2D>();
	}
	void Update () {
		rig.velocity = Vector3.left * Scenes.speedMultip;
		
		if (this.transform.position.x < homo.disappearPointX) {
			Destroy(this.gameObject);
		}
	}
}
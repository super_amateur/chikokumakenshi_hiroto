// PlayerOpration.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class PlayerOperation : MonoBehaviour {
	
	public Roads Rs;
	public Transform enemyAppearPoint;
	public GameObject attackEffect;
	public GameObject defenseEffect;
	// Rigidbody2D rig;
	
	float earlyAttackRang = 2.0f;
	public float attackRang = 2.0f;
	public float attackMultip = 10.0f;
	public float attack = 10.0f;
	public bool isDefence = false;
	// public float jumpPow;
	public Transform swordBody;


	//長岡
	float addMP = 0.005f;
	public GameObject fill;
	public Slider mpSlider;
	public Color startColor;
	public Color endColor;
	public bool isSpecial = false;
	bool isSpecialSound = false;

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine(new Vector3(this.transform.position.x+attackRang, -100, this.transform.position.z), new Vector3(this.transform.position.x+attackRang, 100, this.transform.position.z));
	}

	void Start () {
		// rig = this.GetComponent<Rigidbody2D>();

		//長岡
		mpSlider.value = 0f;
		startColor = Color.yellow;
		endColor   = new Color(1,0.5f,0,1);


		SetSwordRange();
	}
	
	void Update () {
		isSpecial = false;

		GetComponentInChildren<Animator>().SetFloat("speed", Scenes.speedMultip);
		
		attackRang = earlyAttackRang + Scenes.upgrade[1]*0.5f; // アップグレード要素
		// rig.gravityScale = this.transform.position.y+1;
		

		atkSoundWait -= Time.deltaTime;

		//長岡
		if (isDefence) {
			mpSlider.value += addMP;
//			fill.GetComponent<Image> ().color = Color.Lerp (startColor, endColor, mpSlider.value);
			if(mpSlider.value >= 1f && !isSpecialSound){
//				fill = GameObject.Find ("Fill");
				fill.GetComponent<Image> ().color = endColor;
				SpecialSound ();
			}
		}
			
			
	}
	// ダメージ食らう
	public void Damaged(float damage, bool isFire) {
		// ダメージ食らったら赤くなる
		GetComponentInChildren<Animator>().SetTrigger("damaged");
		Scenes.speedMultip = Mathf.Max(1.0f, Scenes.speedMultip-Mathf.Max(damage-Scenes.upgrade[2]/10 , 0.1f));
		Scenes.CreateSound("Voice/HirotoDamage"+(isFire? "Fire": ""));
		Scenes.camera.transform.position += Quaternion.LookRotation(this.transform.position, Scenes.player.transform.position) * Vector3.right * damage;
	}

	public void Damaged(float damage) {
		Damaged(damage, false);
	}
		
	float atkSoundWait = 0f;
	public void SetTap(bool a) {
		isDefence = a;
		if (!a) {
			bool b = false;
			// ディフェンスエフェクト削除
			foreach (Transform defObj in this.GetComponentsInChildren<Transform>()) {
				if (defObj.name.IndexOf(defenseEffect.name+"(Clone)") != -1) {
					Destroy(defObj.gameObject);
					b = true;
				}
			}
			GetComponentInChildren<Animator>().SetBool("defence", false);

			// 攻撃エフェクトの呼び出し
			if (b) {
				if (atkSoundWait <= 0)
				{
					Scenes.CreateSound("Voice/HirotoAttack");
					atkSoundWait = 0.25f;
				}
				Instantiate(attackEffect).transform.parent = this.transform;
				if (!isSpecial) {
					GetComponentInChildren<Animator> ().SetTrigger ("attack");
				}
				foreach (EnemyBehaviour eb in enemyAppearPoint.GetComponentsInChildren<EnemyBehaviour>()) {
					// 敵全員分の繰り返し
					if (eb.transform.position.x < this.transform.position.x+attackRang & eb.transform.position.x > this.transform.position.x) {
						// 攻撃！！！
						eb.Damage(attack + (2 * Scenes.upgrade[0]));
					}
				}
				//長岡
				//ゲージが溜まると必殺技
				if (mpSlider.value >= 1f) {
					GetComponentInChildren<Animator> ().SetTrigger ("special");
					float atkVal = attack * 5 * (Scenes.upgrade[0]+1) * (Scenes.upgrade[1]+1) * (Scenes.upgrade[2]+1);
					foreach (EnemyBehaviour eb in enemyAppearPoint.GetComponentsInChildren<EnemyBehaviour>()) {
						// 敵全員分の繰り返し
						// 攻撃！！！
						eb.Damage(atkVal);
					}
					mpSlider.value = 0f;
					isSpecialSound = false;
					fill.GetComponent<Image> ().color = startColor;
				}
			}
		} else {
			// ディフェンスエフェクトの呼び出し
			GameObject defObj = Instantiate(defenseEffect);
			GetComponentInChildren<Animator>().SetBool("defence", true);
			defObj.transform.parent = this.transform;
			defObj.transform.localPosition = new Vector3(0.5f, -0.3f, 0);
		}
	}

	public void FalseTap() {
		// ディフェンスエフェクト削除
		foreach (Transform defObj in this.GetComponentsInChildren<Transform>()) {
			if (defObj.name.IndexOf(defenseEffect.name+"(Clone)") != -1)
				Destroy(defObj.gameObject);
		}
		GetComponentInChildren<Animator>().SetBool("defence", false);
		isDefence = false;
		GameObject.Find ("ActionButton").GetComponent<Image> ().sprite = Resources.Load<Sprite> ("game/Textures/UI/Game_AttackButton");
	}

	public void SetSwordRange()
	{
		swordBody.localScale = new Vector3(1f + Scenes.upgrade[1]*1f, 1f, 1f);
	}


	//長岡
//	public void SpecialTap(){
//		if (mpSlider.value >= 1f) {
////			isSpecial = true;
//			GetComponentInChildren<Animator> ().SetTrigger ("special");
//			if (!isSpecial) {
//				mpSlider.value = 0f;
//				isSpecialSound = false;
//				fill.GetComponent<Image> ().color = startColor;
//			}
//		}
//	}

	public void SpecialSound(){
		Scenes.CreateSound("SE/special");
		isSpecialSound = true;
	}

}
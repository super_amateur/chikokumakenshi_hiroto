﻿// GUISystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class GUISystem : MonoBehaviour {
	
	private Vector3 position;
	public Vector3 hidden;
	public int sceneNm;
	public bool noMove;
	public bool debug;
	
	RectTransform RT;
	
	public bool active;
	
	const float speed = 10;

	MaskableGraphic[] raycastObject;

	void Start () {
		RT = this.GetComponent<RectTransform>();
		position = RT.localPosition;
		RT.localPosition = hidden;

		int i_01 = 0;
		foreach(MaskableGraphic obj in this.GetComponentsInChildren<MaskableGraphic>()) { if (obj.raycastTarget) i_01++; }
		raycastObject = new MaskableGraphic[i_01];
		// Scenes.raycastObject = new MaskableGraphic[i_01+1];
		i_01 = 0;
		foreach(MaskableGraphic obj in this.GetComponentsInChildren<MaskableGraphic>())
		{
			if (obj.raycastTarget) {
				raycastObject[i_01] = obj;
				// Scenes.raycastObject[i_01] = obj;
				i_01++;
			}
		}
	}
	
	void Update () {
		if (active) {
			RT.position += (position - RT.position)/speed;
			if (Vector3.Distance(position, RT.position) < 0.1f | noMove) {
				foreach (MaskableGraphic obj in raycastObject)
				{
					try {
						((Text)obj).raycastTarget = true;
					} catch (System.Exception) {
						((Image)obj).raycastTarget = true;
					}
				}
			}
		} else {
			if (noMove) {
				this.gameObject.SetActive(false);
			}
			RT.position += (hidden - RT.position)/speed;
			if (Vector3.Distance(hidden, RT.position) < 1f & !debug) {
				this.gameObject.SetActive(false);
			}
		}
	}
}
﻿// ResultSystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class ResultSystem : MonoBehaviour
{
	[SerializeField]
	Text TimeText;
	[SerializeField]
	Text ScoreText;

	public void SetResult () {
		float time = Scenes.time;
		int score = Mathf.RoundToInt(Scenes.score);
		TimeText.text = Mathf.FloorToInt(time/60).ToString("00") +":"+ Mathf.FloorToInt(time%60).ToString("00") +"'"+ Mathf.FloorToInt((time%1)*100).ToString("00");
		TimeText.fontSize = 35;
		TimeText.color = Color.black;
		if (Scenes.time >= Scenes.limitTime) {
			// タイムオーバーなったら
			TimeText.text = "TIME OVER";
			TimeText.fontSize = 20;
			TimeText.color = Color.red;
		}
		ScoreText.text = score.ToString();
	}
	
	void Update () {
		
	}
}
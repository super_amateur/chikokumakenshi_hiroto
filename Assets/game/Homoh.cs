﻿// Homoh.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class Homoh : MonoBehaviour {
	
	[SerializeField]
	public GameObject[] enemy;
	public GameObject goal;
	public GameObject moji;
	public Roads Rs;
	
	public bool goalin;
	
	CsvReader CR;
	int enemyNm = 0;
	float enemyReaderDist = 0;
	
	public float disappearPointX = -20.0f;
	
	public int mojiIntervalDistance;
	[SerializeField]
	int mojiNm;

	void Start() {
		CR = GetComponent<CsvReader>();
		SetEnemy();
	}
	
	public void SetEnemy () {
		enemyReaderDist = 0;
		enemyNm = 0;
		CR.getCsvData();
		mojiNm = mojiIntervalDistance;
	}
	
	void Update () {
		
		if (Scenes.SceneNm == 1) {
			enemyReaderDist += Scenes.speedMultip * Time.deltaTime;
		}
		else
		{
			enemyNm = CR.data.Length -1;
			enemyReaderDist = float.Parse(CR.data[enemyNm][0]);
		}
		
		if (enemyReaderDist > float.Parse(CR.data[enemyNm][0]) & !goalin) {
			if (CR.data[enemyNm][1] == "end") {
				enemyReaderDist = 0;
				enemyNm = 0;
				CR.getCsvData();
				return;
			}
			GameObject newEnm = Instantiate(enemy[int.Parse(CR.data[enemyNm][1])]);
			newEnm.transform.parent = this.transform;
			newEnm.transform.localPosition = new Vector3(0, newEnm.transform.localPosition.y, 0.05f);
			newEnm.GetComponent<EnemyBehaviour>().homo = this;
			enemyNm++;
		}
		if (Rs.playDistance > Rs.goal-this.transform.position.x + Scenes.player.transform.position.x & !goalin) {
			GameObject newEnm = Instantiate(goal);
			newEnm.transform.parent = this.transform;
			newEnm.transform.localPosition = new Vector3(0, newEnm.transform.localPosition.y, 0.05f);
			newEnm.GetComponent<ObjectBehaviour>().homo = this;
			goalin = true;
		}
		
		if (Rs.playDistance > mojiNm +(-this.transform.position.x + Scenes.player.transform.position.x) & Scenes.SceneNm == 1) {
			GameObject newEnm = Instantiate(moji);
			newEnm.transform.parent = this.transform;
			newEnm.transform.localPosition = new Vector3(0, newEnm.transform.localPosition.y, 0.05f);
			newEnm.GetComponent<ObjectBehaviour>().homo = this;
			newEnm.transform.FindChild("body").GetComponent<TextMesh>().text = mojiNm + "M";
			newEnm.transform.FindChild("body").FindChild("mojiShadow").GetComponent<TextMesh>().text = mojiNm + "M";
			mojiNm += mojiIntervalDistance;
		}
	}
}
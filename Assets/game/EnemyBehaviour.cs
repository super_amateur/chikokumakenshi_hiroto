// EnemyBehaviour.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class EnemyBehaviour : MonoBehaviour {
	
	[RangeAttribute(0.0f, 3.0f)]
	public float speed = 1.0f;
	public float damage = 1;
	public float addSpeedMultip = 1;
	[SerializeField]
	int score = 10;
	[SerializeField]
	float hp = 20.0f;

	public string voiceEffectName = "";
	
	public Rigidbody2D rig;
	public Homoh homo;
	
	Transform player;
	RectTransform shadow;
	Transform body;
	Transform dead;
	Animator anm;
	
	bool directAttack;
	bool isDead;
	
	void Start() {
		rig = this.gameObject.GetComponent<Rigidbody2D>();
		shadow = this.transform.FindChild("shadow").GetComponent<RectTransform>();
		body = this.transform.FindChild("body").transform;
		player = Scenes.player.transform;
		anm = this.GetComponentInChildren<Animator>();
	}
	void Update () {
		anm.SetFloat("speed", Scenes.speedMultip / 5);
		rig.velocity = Vector3.left * Scenes.speedMultip + Vector3.left * (isDead? 0: speed) * Scenes.speed;
		Vector3 shadowPos = this.transform.position;
		shadowPos.x = body.transform.position.x;
		shadowPos.y = 0.51f;
		shadow.position = shadowPos;
		
		if (this.transform.position.x < homo.disappearPointX)
		{ // 一定の座標を超えたら消える。
			Destroy(this.gameObject);
		}


		//長岡
		// if (Scenes.player.isSpecial) 
		{ // 必殺技に当たると...
			// Damage(100f + UnityEngine.Random.value * Scenes.upgrade[0] * Scenes.upgrade[1] * Scenes.upgrade[2]);
			// Scenes.CreateSound("SE/special_hit");
			// Scenes.CreateSound("Voice/"+voiceEffectName+"Damage");
			// Destroy (this.gameObject);
		}
		
		if (!directAttack & this.transform.position.x < player.transform.position.x & !isDead & !Scenes.player.isSpecial)
		{ // プレイヤーにダイレクトアタック！
			directAttack = true;
			Scenes.player.Damaged(damage);
			if (Scenes.player.isDefence) {
				Scenes.CreateSound("Player/BreakDefence");
				Scenes.player.FalseTap();
			}
			Scenes.CreateSound("Player/DirectDamage");

//			GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("game/Prefabs/Number"), transform.position, Quaternion.identity);
//			NumberSystem ns = go.GetComponent<NumberSystem>();
//			ns.numberValue = (damage).ToString();
//			ns.textColor = Color.red;
		}
	}
	
	public bool Damage(float dmg) {
		if (isDead) return false;

		hp -= dmg;
		anm.SetTrigger("damaged");
		if (voiceEffectName != "")
			Scenes.CreateSound("Voice/"+voiceEffectName+"Damage");
		if (hp <= 0) {
			Dead();
		}
		GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("game/Prefabs/Number"), this.transform.position, Quaternion.identity);
		NumberSystem ns = go.GetComponent<NumberSystem>();
		ns.numberValue = Mathf.FloorToInt(Mathf.FloorToInt(dmg)).ToString();
		ns.textColor = Color.red;
		return true;
	}
	
	void Dead() {
		if (isDead) return;
		
		// 仮の処理。Animatorで制御したい
		anm.SetBool("dead", true);
		
		GameObject maryoku = Instantiate(Resources.Load<GameObject>("game/Prefabs/Maryoku"));
		maryoku.transform.parent = homo.transform;
		maryoku.transform.position = this.transform.position;
		
		Scenes.speedMultip += addSpeedMultip;
		Scenes.score += score;
		
		isDead = true;
	}
}
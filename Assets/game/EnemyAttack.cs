﻿// EnemyGolem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;
using OriginalNamespace;

public class EnemyAttack : MonoBehaviour {
	
	public MinMax attackInterval = new MinMax(1, 2);
	public MinMax attackDistToPlayer = new MinMax(10, 1);
	public float attackRang = 3;
	public float attackVal = 10;
	public float attackSpeed = 1;
	public GameObject attackEffect;
	public string soundEffects = "";
	Animator anim;
	
	float intv;
	
	void Start () {
		setAttackItvl();
		anim = this.gameObject.GetComponentInChildren<Animator>();
	}
	
	void Update () {
		intv -= Time.deltaTime * Scenes.speed;

		if (intv<=0 & attackDistToPlayer.isInIt(this.transform.position.x- Scenes.player.transform.position.x))
		{ // 攻撃！
			anim.SetTrigger("attack");
			GameObject atkEfx = Instantiate(attackEffect);
			atkEfx.transform.position = this.transform.position;
			atkEfx.transform.parent = this.transform;
			if (GetComponent<EnemyBehaviour>().voiceEffectName != "")
				Scenes.CreateSound("Voice/"+GetComponent<EnemyBehaviour>().voiceEffectName+"Attack");
			
			EnemyAttackEffects EAE = atkEfx.AddComponent<EnemyAttackEffects>();
			EAE.attackRang = attackRang;
			EAE.attackVal = attackVal;
			EAE.attackSpeed = attackSpeed;
			EAE.soundEffect = soundEffects;
//
			setAttackItvl();
		}
	}
	
	void setAttackItvl() {
		intv = attackInterval.Random;
	}
}

/// <summary>
/// 敵の攻撃の飛んでくるやつ
/// </summary>
class EnemyAttackEffects : MonoBehaviour {
	public float attackRang;
	public float attackVal;
	public float attackSpeed;

	public string soundEffect;

	void Start()
	{
		if (soundEffect != "")
		{
			Scenes.CreateSound("enemy/"+soundEffect);
		}
	}
	
	float dist = 0;
	void Update() {
		this.transform.position += Vector3.left * (((attackSpeed * Scenes.speed) + Scenes.speedMultip) * Time.deltaTime);
		dist += (attackSpeed + Scenes.speedMultip) * Time.deltaTime;
		if (dist >= attackRang) {
			Destroy(this.gameObject);
			return;
		}
		
		if (Scenes.player.isDefence & this.transform.position.x < Scenes.player.transform.position.x + Scenes.player.attackRang * 0.8) {
			Scenes.CreateSound("Player/Defence");
			Destroy(this.gameObject);
		}
		if (this.transform.position.x < Scenes.player.transform.position.x) {
			// プレイヤーに当たったら
			if (!Scenes.player.isDefence && !Scenes.player.isSpecial) {
				Scenes.player.Damaged(attackVal, soundEffect.IndexOf("elf") != -1);
				Scenes.CreateSound("Player/Damage");
//				GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("game/Prefabs/Number"), transform.position, Quaternion.identity);
//				NumberSystem ns = go.GetComponent<NumberSystem>();
//				ns.numberValue = (attackVal).ToString();
//				ns.textColor = Color.red;
			}
			Destroy (this.gameObject);
		}
	}
}
﻿// MaryokuBehavir.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class MaryokuBehaviour : MonoBehaviour
{
	GameObject mesh;
	Collider2D collider;
	Rigidbody2D rig;

	float rotateSpeed = 60;
	float meshFuwafuwaSpeed = 1.5f;

	void Start () {
		mesh = this.GetComponentInChildren<MeshRenderer>().gameObject;
		collider = this.GetComponentInChildren<Collider2D>();
		rig = this.GetComponent<Rigidbody2D>();

		this.transform.Rotate(0, Random(360), 0);

		rig.velocity = new Vector3(0, Random(3, 5), 0);

		rotateSpeed = Random(30, 180) * (Random(2)<1? -1: 1);
		meshFuwafuwaSpeed = Random(1.5f, 2.5f);
	}
	
	void Update () {
		rotateSpeed -= rotateSpeed * (1/3) * Time.deltaTime;
		this.transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);

		mesh.transform.localPosition = new Vector3(0, Mathf.Sin(Time.time * meshFuwafuwaSpeed)*0.1f + 0.1f, 0);

		rig.velocity = Vector3.left * Scenes.speedMultip + Vector3.up * rig.velocity.y;
		
		if (this.transform.position.x < Scenes.player.transform.position.x) {
			// プレイヤーに当たったら
			Scenes.maryoku++;
			Scenes.CreateSound("Player/GetMagic");

			GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("game/Prefabs/Number"), Scenes.player.transform.position, Quaternion.identity);
			NumberSystem ns = go.GetComponent<NumberSystem>();
			ns.numberValue = "+1";
			ns.textColor = new Color(255, 0, 255);

			Destroy (this.gameObject);
		}
	}

	float Random(float max) {
		return Random(0, max);
	}
	float Random(float min, float max) {
		return UnityEngine.Random.value * (max - min) + min;
	}
}
﻿// Roads.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class Roads : MonoBehaviour {
	
	public GameObject road;
	public GameObject player;
	public Slider distSlider;
	
	public float appearPosX;
	public float disappearPosX;
	
	public float goal = 200;
	public float playDistance = 0;

	public float playerAnimationSpeed = 0.25f;
	
	void Start () {
		disappearPosX -= 1-(appearPosX - disappearPosX)%1;
		for (int i=0; i<(appearPosX-disappearPosX); i++) {
			GameObject go = Instantiate(road);
			go.transform.parent = this.transform;
			go.transform.localPosition = Vector3.right * (appearPosX - i);
			Road rd = go.AddComponent<Road>();
			rd.Rs = this;
		}
	}
	
	void Update() {
		// ゲームシーンの時は
		if (Scenes.SceneNm == 1) {
			Scenes.speedMultip += 0.25f*Time.deltaTime;
			playDistance += Scenes.speedMultip * Time.deltaTime;
			Scenes.score += Scenes.speedMultip * Time.deltaTime;
			distSlider.value = playDistance/goal;
			if (playDistance > goal) {
				Scenes.CreateSound("SE/gameclear");
				Scenes.clearStatus = 0;
				Scenes.SceneNm = 2;
			}
		}
		else
		{
			Scenes.speedMultip -= (Scenes.speedMultip -1)/300;
		}
		player.GetComponentInChildren<Animator>().SetFloat("speed", (Scenes.speedMultip-1)*playerAnimationSpeed+1);
		// Debug.Log(player.GetComponentInChildren<Animator>().name);
	}
}

public class Road : MonoBehaviour {
	public Roads Rs;
	
	void Update() {
		this.transform.localPosition += Vector3.left * Scenes.speedMultip * Time.deltaTime;
		if (this.transform.localPosition.x < Rs.disappearPosX) {
			this.transform.localPosition = Vector3.right * (Rs.appearPosX + (this.transform.localPosition.x - Rs.disappearPosX));
		}
	}
}
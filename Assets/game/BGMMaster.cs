﻿// BGMMaster.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class BGMMaster : MonoBehaviour
{
	[SerializeField]
	BGM[] bgm;

	AudioSource AS;

	void Start () {
		AS = GetComponent<AudioSource>();
	}
	
	void Update () {
		
	}

	public void ChangeBGM() {
		foreach (BGM i in bgm) {
			if (Scenes.SceneNm == i.sceneNm) {
				AS.clip = i.AC;
				if (!AS.isPlaying) {
					AS.Play();
				}
				break;
			}
		}
	}
}

[System.SerializableAttribute]
struct BGM {
	[SerializeField]
	public AudioClip AC;
	[SerializeField]
	public int sceneNm;
}
﻿// ShopSystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class ShopSystem : MonoBehaviour
{
	public Text haveMaryoku;
	public GameObject[] Upgrade = new GameObject[3];

	void Start () {
		// int i = 0;
		// foreach (GameObject go in Upgrade) {
		// 	UpgradeBehaviour ub = go.GetComponent<UpgradeBehaviour>();
		// 	ub.shopID = (i++);
		// }
	}
	
	void Update () {
		haveMaryoku.text = Scenes.maryoku.ToString();
	}

	public void SetStore() {
		foreach (GameObject up in Upgrade) {
			up.GetComponent<UpgradeBehaviour>().SetStore();
		}
	}
}
﻿// ScenesMaster.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public static class Scenes {
	
	public static float speed = 1;
	static float spm = 1;
	public static float speedMultip{
		set { spm = Mathf.Max(Mathf.Min(value, 20.0f), 1.0f); }
		get { return spm * speed; }
	}
	static public float time = 0;
	static public float limitTime;

	static public float score = 0;

	//////////// セーブデータ関連
	static public int maryoku = 0;
	static public int[] upgrade = new int[3];
	static bool updateData;
	static public void DataUpdate() {updateData = true;}
	////////////

	static public PlayerOperation player;
	static public Roads road;
	static public CameraMove camera;



	static public UnityEngine.Audio.AudioMixerGroup VoiceGroup;
	static public GameObject CreateSound (string audioClip) {
		GameObject go = MonoBehaviour.Instantiate(Resources.Load<GameObject>("game/Prefabs/SoundEffect"));
		go.name = audioClip;

		AudioClip ac;
		if (!(ac = Resources.Load<AudioClip>("game/Sounds/"+audioClip))){
			int i = 0;
			while (Resources.Load<AudioClip>("game/Sounds/"+audioClip+"_"+i.ToString("00"))) { i++; }
			ac = Resources.Load<AudioClip>("game/Sounds/"+audioClip+"_"+(Mathf.FloorToInt(UnityEngine.Random.value * i)).ToString("00"));
		}

		Debug.Log("SE:"+ac.name);
		if (audioClip.IndexOf("Voice/") != -1) {
			go.GetComponent<AudioSource>().outputAudioMixerGroup = VoiceGroup;
			// Debug.Log("キャラクターボイス");
		}
		go.GetComponent<AudioSource>().clip = ac;
		go.GetComponent<AudioSource>().Play();
		go.transform.parent = GameObject.Find("BGMMaster").transform;
		return go;
	}




	
	static int scnm = 0;
	static public GUISystem[] GUISys;
	static public MaskableGraphic[] raycastObject;
	static public int clearStatus = -1;
	public static int SceneNm {
		set {
			speed = (value==5? 0: 1);

			CreateSound("SE/UI/changeUI");

			if (updateData) {
				PlayerPrefs.Save();
				updateData = false;
			}

			if (scnm == 1 & value != 5) {
				Scenes.player.FalseTap();
			}
			
			// カメラ位置の変更
			GameObject.Find("Main Camera").GetComponent<CameraMove>().num = value;
			
			// 画面背景色の変更
			var camera = GameObject.Find("Main Camera").GetComponent<CameraMove>();
			camera.num = value;
			float ch, cs, cv;
			Color.RGBToHSV(camera.GetComponent<Camera>().backgroundColor, out ch, out cs, out cv);
			cs = 0.5f;
			cv = 1.0f;
			
			int colID = value;
			if (value == 6) colID = 1;
			if (value < 4) {
				camera.color = Color.HSVToRGB(cityColor[colID]/360, cs, cv);

				// 背景画像色の変更
				int i=1;
				foreach (MoveCity mc in GameObject.Find("BackCity").GetComponentsInChildren<MoveCity>()) {
					i++;
					float h, s, v;
					Color.RGBToHSV(mc.color, out h, out s, out v);
					s = mc.Saturation;
					v = 1.0f;
					mc.color = Color.HSVToRGB(cityColor[colID]/360, s, v);
				}
			}
			
			// GUIの制御
			for (int i = 0; i < GUISys.Length; i++) {
				GUISys[i].active = (value == GUISys[i].sceneNm);
				if (value == GUISys[i].sceneNm)
					GUISys[i].gameObject.SetActive(value == GUISys[i].sceneNm);
			}
			foreach (MaskableGraphic raiObj in raycastObject)
			{
				Debug.Log("off");
				raiObj.raycastTarget = false;
			}

			if (value == 0 & scnm != 4) {
				// タイトル時
				foreach (EnemyBehaviour eb in GameObject.Find("EnemyAppearPoint").GetComponentsInChildren<EnemyBehaviour>()) {
					MonoBehaviour.Destroy (eb.gameObject);
				}
				CreateSound("Voice/TitleCall");
				player.SetSwordRange();
			}
			
			if (value == 1 & scnm != 5) {
				// ゲーム開始
				GameObject.Find("Roads").GetComponent<Roads>().playDistance = 0;
				speedMultip = 1;
				time = 0;
				score = 0;
				GameObject.Find("EnemyAppearPoint").GetComponent<Homoh>().goalin = false;
				CreateSound("Voice/GameStart");
				player.FalseTap ();
				player.mpSlider.value = 0f;
				player.fill.GetComponent<Image> ().color = player.startColor;
			}

			if (value == 2) {
				// ゲーム終了時
				PlayerPrefs.SetInt("maryoku", maryoku);
				GameObject.Find("2_ResultBottom").GetComponent<ResultSystem>().SetResult();
				GameObject.Find("EnemyAppearPoint").GetComponent<Homoh>().SetEnemy();
				if (scnm == 1) {
					switch (clearStatus) {
						case 0:
							CreateSound("Voice/GameClear");
							break;
						case 1:
						case 2:
							CreateSound("Voice/GameOver");
							break;
					}
				}
			}
			if (scnm == 2) {
				// リザルトから抜ける時
				clearStatus = -1;
			}

			if (value == 3) {
				// ショップ入店時
				GameObject.Find("3_Shop").GetComponent<ShopSystem>().SetStore();
			}

			scnm = value;

			GameObject.Find("BGMMaster").GetComponent<BGMMaster>().ChangeBGM();
		}
		get {
			return scnm;
		}
	}

	public static float[] cityColor = new float[]{
		120,
		221,
		30,
		300
	};
}
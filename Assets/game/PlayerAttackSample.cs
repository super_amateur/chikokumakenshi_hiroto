﻿// PlayerAttackSample.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class PlayerAttackSample : MonoBehaviour {
	
	public float effectTime = 0.5f;
	
	float dist;
	
	void Start() {
		this.transform.position = new Vector3(Scenes.player.transform.position.x, 1, 0);
		dist = Scenes.player.attackRang;
	}
	
	void Update () {
		float a;
		this.transform.position += Vector3.right * (a = Time.deltaTime * Scenes.speed * Scenes.player.attackRang / effectTime);
		
		dist -= a;
		
		if (dist < 0) {
			Destroy(this.gameObject);
		}
	}
}

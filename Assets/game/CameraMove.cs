﻿// CameraMove.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class CameraMove : MonoBehaviour {
	
	public Transform[] point = new Transform[1];
	public int num = 0;
	public Transform target;
	[RangeAttribute(1, 100)]
	public float speed = 10;
	public Color color = Color.white;

	void Awake() {
		Scenes.camera = this;
	}
	
	void Start() {
		this.transform.position = point[num].transform.position;
		target.position = point[num].transform.rotation * point[num].transform.position + point[num].transform.position;
		this.transform.LookAt(target);
		
		// 様々な画面サイズに対して横幅を基準にカメラの広角で画面サイズを合わせる。
		// 要は、道の端やプレイヤーなどの見切れを防ぐのだ! ﾊﾞｰﾝ!!
		this.GetComponent<Camera>().fieldOfView = 60 * (((float)Screen.height/(float)Screen.width) * (9f/16f));
	}
	
	void Update () {
		this.transform.position += (point[num].transform.position - this.transform.position) * speed / 100;
		Vector3 nextTarget = point[num].transform.rotation * point[num].transform.position + point[num].transform.position;
		target.position += (nextTarget - target.transform.position) * speed / 100;
		this.transform.LookAt(target);

		Color nowC = this.GetComponent<Camera>().backgroundColor;
		this.GetComponent<Camera>().backgroundColor = new Color(nowC.r + (color.r - nowC.r)/20, nowC.g + (color.g - nowC.g)/20, nowC.b + (color.b - nowC.b)/20);
	}
}
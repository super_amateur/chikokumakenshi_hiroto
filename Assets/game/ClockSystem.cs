﻿// ClockSystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class ClockSystem : MonoBehaviour
{
	public RectTransform secPin;
	public RectTransform minPin;
	public RectTransform horPin;
	
	// public float time; // *sec
	// public float limitTime;
	
	void Start () {
		// time = 0f;
	}
	
	void Update () {
		// time += Time.deltaTime;
		float realTime = 32400 - Scenes.limitTime + Scenes.time; // 9時
		// float realTime = 32400 - limitTime + time;
		
		secPin.localRotation = Quaternion.Euler(0, 0, -(int)(realTime % 60) * 6);
		minPin.localRotation = Quaternion.Euler(0, 0, -(realTime / 60) % 60 * 6);
		horPin.localRotation = Quaternion.Euler(0, 0, -(realTime / 3600) % 12 * 30);

		if (Scenes.limitTime < Scenes.time & Scenes.SceneNm == 1) {
			Scenes.CreateSound("SE/gameclear");
			Scenes.time = Scenes.limitTime;
			Scenes.clearStatus = 1;
			Scenes.SceneNm = 2;
		}
	}
}
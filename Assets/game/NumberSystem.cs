﻿// NumberSystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class NumberSystem : MonoBehaviour
{
	public float lifeTime = 1.0f;
	public string numberValue;
	public Color textColor;
	public Rigidbody2D rig;

	void Start() {
		this.transform.parent = GameObject.Find("BGMMaster").transform;
		GetComponent<TextMesh>().text = numberValue;
		GetComponent<TextMesh>().color = textColor;
		rig = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
		lifeTime -= Time.deltaTime;
		if (lifeTime <= 0) {
			Destroy(this.gameObject);
		}
	}
}
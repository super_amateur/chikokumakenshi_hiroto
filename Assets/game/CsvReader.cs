﻿// CsvReader.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class CsvReader : MonoBehaviour {
	
	[HeaderAttribute("ここに的譜面を入れていくと")]
	[HeaderAttribute("ランダムに譜面を読み込む。")]
	public TextAsset[] srcCsv;
	
	public string[][] data;

	[SerializeField]
	int csvNm = 0;
	
	// void Update () {}
	
	public void getCsvData() {
		Debug.Log("getCSVData");
		csvNm = Mathf.FloorToInt(Mathf.Min(Scenes.speedMultip / 20, 0.99999f) * srcCsv.Length);
		TextAsset asset = srcCsv[csvNm];
		string[] a = asset.text.Split('\n');
		string[][] b = new string[a.Length][];
		
		for (int i=0; i<a.Length; i++) {
			b[i] = a[i].Split(',');
		}
		
		data = b;
	}
}
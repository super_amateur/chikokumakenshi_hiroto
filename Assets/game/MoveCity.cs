﻿// MoveCity.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using System.Collections.Generic;

public class MoveCity : MonoBehaviour {
	
	public float speed = 1;
	public Color color = Color.white;
	public float Saturation;
	Roads Rs;

	void Start () {
		Rs = GameObject.Find("Roads").GetComponent<Roads>();
		this.GetComponent<MeshRenderer>().material.mainTextureOffset += Vector2.right * Random.value;
		this.GetComponent<MeshRenderer>().material.color = color;
	}
	
	void Update () {
		// float th, ts, tv;
		// Color.RGBToHSV(color, out th, out ts, out tv);
		// float nh, ns, nv;
		// Color.RGBToHSV(this.GetComponent<MeshRenderer>().material.color, out nh, out ns, out nv);
		// if (this.name == "city01") Debug.Log(th+":"+nh);
		// this.GetComponent<MeshRenderer>().material.color = Color.HSVToRGB(nh + (th - nh)/30, ns, nv);
		Color nowC = this.GetComponent<MeshRenderer>().material.color;
		this.GetComponent<MeshRenderer>().material.color = new Color(nowC.r + (color.r - nowC.r)/20, nowC.g + (color.g - nowC.g)/20, nowC.b + (color.b - nowC.b)/20);
		
		this.GetComponent<MeshRenderer>().material.mainTextureOffset += Vector2.right * speed * Scenes.speedMultip * Time.deltaTime;
	}
}

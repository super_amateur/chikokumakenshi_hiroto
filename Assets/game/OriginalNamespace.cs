﻿// OriginamNamespace.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

namespace OriginalNamespace
{
	[System.SerializableAttribute]
	public struct MinMax
	{
		/// 最大値最小値に特化した型。
		public MinMax(float min, float max) {
			Min = Mathf.Min(min, max);
			Max = Mathf.Max(min, max);
		}
		
		[SerializeField]
		float Min;
		public float minData {
			set {
				if (Max > value) {
					Min = Max;
					Max = value;
				} else {
					Min = value;
				}
			}
			get {
				return Min;
			}
		}
		[SerializeField]
		float Max;
		public float maxData {
			set {
				if (Min < value) {
					Max = Min;
					Min = value;
				} else {
					Max = value;
				}
			}
			get {
				return Max;
			}
		}
		
		public float Random {
			get {
				return (Min + UnityEngine.Random.value * (Max - Min));
			}
		}
		
		public float Distance {
			get {
				return (Max - Min);
			}
		}
		
		public bool isInIt(float val) {
			return (val < Max & val > Min);
		}
	}
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class SaveText : MonoBehaviour {

	string str;
	private string name;
	public InputField inputField;


	string check;
	void Update(){
		inputField.text = inputField.text.ToUpper ();
		if (inputField.text.Length > 8) {
			inputField.text = inputField.text.Substring(0, 8);
		}

		string ch = inputField.text;
		if (new Regex(@"[a-zA-Z0-9]{1,}").Replace(ch, "") == "") {
			check = ch.ToUpper();
		} else {
			Debug.Log("使えない文字！");
			inputField.text = check;
		}

		name = inputField.text;

	}

	public string GetName(){
		return name;
	}
}

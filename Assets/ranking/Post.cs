﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

// using MySql.Data;
// using MySql.Data.MySqlClient;

public class Post : MonoBehaviour {

	string url = "http://mijinko-unpro.chu.jp/chikoku-ma-kenshi/test01/test2.php";

	[SerializeField]
	GUISystem AddRanking;
	[SerializeField]
	GUISystem OffLine;

	const int ReloadNm = 3; //ランキング再読み込み回数
	const int RankingNm = 15; // ランキングの最大データ数

	public bool addedRanking;

	[SerializeField]
	GameObject RankingView;

	void Start(){
		// StartCoroutine (Ranking ());
	}
		
	void Update(){
		// if(Input.GetKeyDown(KeyCode.Tab)){
		// 	SaveText saveText = GetComponent<SaveText> ();
		// 	StartCoroutine (PostRanking (saveText.GetName(), 20000));
		// }
		// if(Input.GetKeyDown(KeyCode.Space)){
		// 	Debug.Log("object message");
		// 	StartCoroutine (Ranking ());
		// }
		if ((rankings.Length < 2 | rankings.Length > RankingNm+1) & waitCount < ReloadNm) {
		
			if ((waitTime-=Time.deltaTime) < 0) {

				Debug.Log("ランキング読み込み依頼");
				waitCount++;
				StartCoroutine (Ranking ());
				waitTime = 1.0f;
			} 
		} else if (waitCount == ReloadNm) {
			waitCount++;
			/*オフライン時の処理*/
			Debug.Log("オフラインなう！");
			
			AddRanking.active = false;
			OffLine.gameObject.SetActive(true);
			OffLine.active = true;
		}
	}
	float waitTime;
	int waitCount;

	public void SetRanking() {
		StartCoroutine (PostRanking ());

		AddRanking.active = false;
		addedRanking = true;
	}

	// Use this for initialization
	IEnumerator PostRanking () {
		string Name = GetComponent<SaveText> ().GetName();
		Debug.Log(Name);
		PlayerPrefs.SetString("PlayerName", Name);

		int Score = Mathf.RoundToInt(Scenes.score);

		WWWForm form = new WWWForm ();
		form.AddField("Name", Name);
		form.AddField ("Score", Score);
		WWW result = new WWW (url, form.data);
		yield return result;
		if (result.error == null) {
			Debug.Log ("登録完了");
			rankings = new string[0];
			waitCount = 0;
			addedRanking = true;
			this.GetComponent<MakeRanking>().Reset();
		}
	}

	void checkOfIntoRanking() {
		bool intoRanking = false;

		if (rankings.Length > 1)
			intoRanking = int.Parse(rankings[rankings.Length-2].Split('|')[1].Split(':')[1]) < Scenes.score;
		if (rankings.Length < RankingNm)
			intoRanking = true;

		Debug.Log("clearStatus="+Scenes.clearStatus);
		if (Scenes.clearStatus == 0) {
			if (intoRanking & !addedRanking) {
				/* ランキング圏内の場合に名前入力のUIを出す */
				Debug.Log("ランキング圏内！ "+Scenes.score);

				AddRanking.gameObject.SetActive(true);
				AddRanking.active = true;
				GetComponent<SaveText> ().inputField.text = PlayerPrefs.GetString("PlayerName", "");


			} else {
				Debug.Log("ランキング圏外！ "+Scenes.score);

				AddRanking.active = false;
			}
		}
	}

	public void GoToTitleFromResult() {
		Scenes.SceneNm = 0;
		addedRanking = false;
		rankings = new string[0];
	}


	public string[] rankings;

	IEnumerator Ranking () {
		WWW RankingDate = new WWW (url);

		yield return RankingDate;
		string RankingDateString = RankingDate.text;
		Debug.Log ("RankingDateString == '':"+RankingDateString == "");
		rankings = RankingDateString.Split ('\n');

		Debug.Log("rankings.Length = "+rankings.Length);
		if (rankings.Length > 1 & rankings.Length <= RankingNm+1)
			OffLine.active = false;
			checkOfIntoRanking();
	}


}

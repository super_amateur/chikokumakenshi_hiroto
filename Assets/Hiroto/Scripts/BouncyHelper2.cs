﻿// BouncyHelper2.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class BouncyHelper2 : MonoBehaviour {

	const float sphereRadius = 0.1f;

	[SerializeField]
	SpringJoint parentSpring;
	[SerializeField]
	float localAngle_z = 180f;
	[SerializeField]
	float armLength = 1f;
	[SerializeField]
	float bouncy = 1f;

	Quaternion nativeAngle;
	Quaternion conectedAngle;
	Vector3 bornEndPosition;
	Vector3 lastPosition;

	void Awake ()
	{
	}

	void Start ()
	{
		nativeAngle = this.transform.rotation;
		conectedAngle = nativeAngle * Quaternion.Euler(Vector3.forward * localAngle_z);
		bornEndPosition = this.transform.position + conectedAngle * Vector3.up * armLength;
	}
	
	void Update ()
	{
		SimulateBouns();
	}

	void OnDrawGizmos()
	{
		// DrawBornMark();
	}

	void OnDrawGizmosSelected()
	{
		DrawBornMark();
	}



	void GetStatus()
	{
		// if (!EditorApplication.isPlaying)
		{
			// bornEndPosition = this.transform.position + conectedAngle * Vector3.up * armLength;
			// conectedAngle = this.transform.rotation * Quaternion.Euler(Vector3.forward * localAngle_z);
			// lastPosition = bornEndPosition;
		// } else {
			conectedAngle = nativeAngle * Quaternion.Euler(Vector3.forward * localAngle_z);
			lastPosition = bornEndPosition + parentSpring.transform.localPosition * bouncy;
		}
	}

	void SimulateBouns()
	{
		if (!parentSpring) return;

		GetStatus();

		// this.transform.localRotation = Quaternion.FromToRotation(Vector3.up, (lastPosition - this.transform.position).normalized) * Quaternion.Euler(0, 0, -localAngle_z) * nativeAngle;
		this.transform.localRotation = Quaternion.FromToRotation(Vector3.up, (lastPosition - this.transform.position).normalized) * Quaternion.Euler(0, 0, -localAngle_z);
	}

	void DrawBornMark()
	{
		GetStatus();

		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(this.transform.position, sphereRadius);
		Gizmos.DrawLine(this.transform.position, lastPosition);
		Gizmos.DrawLine(lastPosition + conectedAngle * Vector3.left * sphereRadius, lastPosition + conectedAngle * Vector3.right * sphereRadius);
		Gizmos.DrawLine(lastPosition + conectedAngle * Vector3.back * sphereRadius, lastPosition + conectedAngle * Vector3.forward * sphereRadius);
	}

	void DrawAngleDebug(Quaternion angle)
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine(this.transform.position, this.transform.position + angle * Vector3.up);
	}



	Quaternion QuatSub(Quaternion a, Quaternion b)
	{
		return new Quaternion(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
	}
}
